defmodule Schooner do
  use Application

  def start(_type, _args) do
    children = [
      {Schooner.Server, node()},
      {Task.Supervisor, name: Schooner.TaskSupervisor}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
