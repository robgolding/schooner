defmodule Schooner.Server.State do
  defstruct [
    :id,
    role: :follower,
    term: 0,
    voted_for: nil,
    votes_received: 0,
    timer: nil,
    heartbeat: nil,
    peers: []
  ]
end

defmodule Schooner.Server do
  require Logger
  use GenServer

  alias Schooner.Server.State

  @heartbeat_interval 50

  def start_link(id) do
    GenServer.start_link(__MODULE__, %State{id: id}, name: Schooner.Server)
  end

  def init(state) do
    peers =
      Enum.filter(
        Application.get_env(:schooner, :nodes, []),
        fn id -> id != state.id end
      )

    Enum.map(peers, &Node.connect/1)
    {:ok, %State{state | timer: set_timer(), peers: peers}}
  end

  defp election_timeout() do
    # 301 - 1,300ms
    :rand.uniform(1_000) + 300
  end

  defp set_timer() do
    Process.send_after(self(), {:election_timeout}, election_timeout())
  end

  defp reset_timer(%State{timer: timer} = state) do
    Process.cancel_timer(timer)
    %State{state | timer: set_timer()}
  end

  defp set_heartbeat() do
    Process.send_after(self(), {:heartbeat}, @heartbeat_interval)
  end

  defp reset_heartbeat(%State{heartbeat: heartbeat} = state) do
    Process.cancel_timer(heartbeat)
    %State{state | heartbeat: set_heartbeat()}
  end

  def handle_info({:election_timeout}, %State{role: :follower} = state) do
    state =
      state
      |> become_candidate
      |> start_election

    {:noreply, state}
  end

  def handle_info({:election_timeout}, %State{role: :candidate} = state) do
    state =
      state
      |> debug("Election timeout; restarting")
      |> start_election

    {:noreply, state}
  end

  def handle_info({:election_timeout}, %State{role: :leader} = state) do
    # do nothing
    {:noreply, state}
  end

  def handle_info({:heartbeat}, %State{role: :leader} = state) do
    state =
      state
      |> send_heartbeats
      |> reset_heartbeat

    {:noreply, state}
  end

  def handle_info({:heartbeat}, %State{role: :candidate} = state) do
    # do nothing
    {:noreply, state}
  end

  def handle_info({:heartbeat}, %State{role: :follower} = state) do
    # do nothing
    {:noreply, state}
  end

  def handle_cast({:request_vote, {req_term, candidate}}, %State{id: id} = state) do
    debug(state, "Received vote request from #{candidate} for term #{req_term}")

    state =
      if req_term > state.term do
        state
        |> debug("Term out of date in request vote")
        |> become_follower(req_term)
      else
        state
      end

    if req_term == state.term do
      if state.voted_for == nil or state.voted_for == candidate do
        GenServer.cast(
          {Schooner.Server, candidate},
          {:request_vote_reply, {state.term, id, true}}
        )

        {:noreply, reset_timer(%State{state | voted_for: candidate})}
      else
        GenServer.cast(
          {Schooner.Server, candidate},
          {:request_vote_reply, {state.term, id, false}}
        )

        {:noreply, state}
      end
    else
      GenServer.cast({Schooner.Server, candidate}, {:request_vote_reply, {state.term, id, false}})
      {:noreply, state}
    end
  end

  def handle_cast(
        {:request_vote_reply, {reply_term, from, vote_granted}},
        %State{term: term, role: :candidate, votes_received: votes_received} = state
      ) do
    state =
      if reply_term > term do
        state
        |> debug("Term out of date in request vote reply")
        |> become_follower(reply_term)
      else
        if vote_granted do
          state = %State{state | votes_received: votes_received + 1}
          debug(state, "Received #{votes_received + 1} votes so far...")

          if state.votes_received * 2 > Enum.count(state.peers) + 1 do
            become_leader(state)
          else
            state
          end
        else
          debug(state, "Vote from #{from} was not granted :-(")
        end
      end

    {:noreply, state}
  end

  def handle_cast({:request_vote_reply, {_, _, _}}, %State{} = state) do
    {:noreply, state}
  end

  def handle_cast({:append_entries, {req_term, leader, entries}}, %State{} = state) do
    state =
      if req_term > state.term do
        state
        |> debug("Term out of date in append entries")
        |> become_follower(req_term)
      else
        state
      end

    if req_term == state.term do
      state =
        if state.role != :follower do
          become_follower(state, req_term)
        else
          state
        end

      state = reset_timer(state)
      GenServer.cast({Schooner.Server, leader}, {:append_entries_reply, {state.term, true}})
      {:noreply, state}
    else
      GenServer.cast({Schooner.Server, leader}, {:append_entries_reply, {state.term, false}})
      {:noreply, state}
    end
  end

  def handle_cast({:append_entries_reply, {reply_term, success}}, %State{term: term} = state) do
    state =
      if reply_term > term do
        state
        |> debug("Term out of date in heartbeat reply")
        |> become_follower(reply_term)
      else
        state
      end

    {:noreply, state}
  end

  defp become_candidate(%State{term: term, role: :follower} = state) do
    %State{state | role: :candidate}
    |> debug("Becoming candidate")
  end

  defp become_leader(%State{term: term, role: candidate} = state) do
    %State{state | role: :leader, heartbeat: set_heartbeat()}
    |> debug("Becoming leader")
  end

  defp become_follower(%State{} = state, term) do
    %State{state | role: :follower, term: term, voted_for: nil}
    |> reset_timer()
    |> debug("Becoming follower")
  end

  defp start_election(%State{term: term, role: :candidate} = state) do
    state
    |> increment_term
    |> debug("Starting election")
    |> vote_for_self
    |> request_votes
    # |> become_leader
    |> reset_timer
  end

  defp request_votes(%State{peers: peers} = state) do
    Enum.map(peers, &request_vote(state, &1))
    state
  end

  defp request_vote(%State{id: id, term: term} = state, peer) do
    debug(state, "Requesting vote from peer #{peer}")
    GenServer.cast({Schooner.Server, peer}, {:request_vote, {term, id}})
  end

  defp vote_for_self(%State{id: id} = state) do
    %State{state | voted_for: id, votes_received: 1}
  end

  defp send_heartbeats(%State{peers: peers} = state) do
    Enum.map(peers, &send_heartbeat(state, &1))
    state
  end

  defp send_heartbeat(%State{id: id, term: term} = state, peer) do
    GenServer.cast({Schooner.Server, peer}, {:append_entries, {term, id, []}})
  end

  defp increment_term(%State{term: term} = state) do
    %State{state | term: term + 1}
  end

  defp debug(%State{id: id, role: role, term: term} = state, message) do
    Logger.debug(message, id: id, role: role, term: term)
    state
  end
end
