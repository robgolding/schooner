use Mix.Config

config :logger, :console,
  format: "[$date] [$time] [$level] $message $metadata\n",
  metadata: [:error_code, :id, :role, :term]

config :schooner,
  nodes: [:node1@nylon, :node2@nylon, :node3@nylon]
