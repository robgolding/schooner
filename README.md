# Schooner

Schooner is an implementation of the
[Raft consensus algorithm](https://raft.github.io/) in Elixir (yes, another one!)

It's still a work in progress; leadership election is currently implemented and
a 3-node cluster is assumed.
